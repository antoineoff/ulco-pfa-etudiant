data List a = Empty | Cons a (List a)

sumList :: Num a => List a -> a
sumList Nil = 0
sumList (Cons x xs) = x + sumList xs 


concatList  :: List a -> List a -> List a 
concatList Nil ys = ys
concatList (Cons x xs) ys = Cons x (concatList xs ys)

toHaskell :: List a -> [a]
toHaskell Empty = []
toHaskell (Cons h t) = h:toHaskell t 



-- fromHaskell 

-- myShowList

main :: IO ()
main = do
    print ((Cons 1 (Cons 2 Nil)) :: List Int)
    print ((Cons True (Cons False Nil)) :: List Bool)
    print (sumlist (Cons 1 (COns 2 Nil)))

