
data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche

estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables = length . filter (not . estWeekend)

main :: IO ()
main = do 
        print $ estWeekend Dimanche
        print $ estWeekend Mardi
        print $ compterOuvrables [Lundi, Mardi, Mercredi, Jeudi, Vendredi, Samedi, Dimanche]


