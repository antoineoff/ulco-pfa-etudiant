{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson
import qualified Data.Text as T
import GHC.Generics

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Generic, Show)

instance fromJSON Person

main :: IO ()
main = do
    res0 <- decodeFileStrict "aeson-test1.json"
    print (res0 ::  Either String Person) 

