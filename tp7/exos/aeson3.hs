{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)


instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person 
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: "birthyear"

main :: IO ()
main = do
    let res0 = Person "John" "Doe" 1970
    print res0

